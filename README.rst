Development configuration for publik
====================================

An bunch of Ansible playbooks that install and setup a multitenant publik instance using sources.

A complete version of the documentation is maintained here:
https://dev.entrouvert.org/projects/publik-devinst/wiki/Installation_d%27un_environnement_de_d%C3%A9veloppement_local
