ifneq ("$(wildcard local-inventory.yml)","")
    INVENTORY ?= local-inventory.yml
else
    INVENTORY ?= inventory.yml
endif

# Debian no longer provide openssl-provider-legacy by default and we don't use it
# see https://github.com/pyca/cryptography/issues/11450
CRYPTOGRAPHY_OPENSSL_NO_LEGACY=1
export CRYPTOGRAPHY_OPENSSL_NO_LEGACY

ASKPASS ?= -K

help:
	@echo ""
	@echo "make install: install/reinstall a Publik system"
	@echo "make deploy: deploy a *.dev.publik.love Publik instance"
	@echo "make renew-certificate: get *.dev.publik.love valid certificate"
	@echo "make upgrade: pull new Publik code and do all migrations"
	@echo ""
	@echo "To use a specific inventory (default is local-inventory.yml or inventory.yml):"
	@echo "  make INVENTORY=my-inventory.yml ..."
	@echo ""
	@echo "More details on https://doc-publik.entrouvert.com/dev/installation-developpeur/"
	@echo ""

install:
	ansible-playbook $(ASKPASS) -i $(INVENTORY) install.yml

deploy:
	ansible-playbook -i $(INVENTORY) deploy-tenants.yml

renew-certificate:
	ansible-playbook $(ASKPASS) -i $(INVENTORY) --tags "tls" install.yml

upgrade:
	ansible-playbook -i $(INVENTORY) --tags "source" install.yml

delete:
	ansible-playbook -i $(INVENTORY) delete-tenants.yml

clean:
	ansible-playbook $(ASKPASS) -i $(INVENTORY) clean.yml

tests: install deploy
