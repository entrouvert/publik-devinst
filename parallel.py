#!python3 -u
import subprocess
import argparse
from concurrent.futures import ThreadPoolExecutor

TIMEOUT = 60 * 30  # 30 minutes


def process(args):
    arg, cmd = args
    print(f'{arg} :', cmd)
    try:
        subprocess.check_call(cmd, timeout=TIMEOUT)
    except subprocess.CalledProcessError:
        return arg, 1
    else:
        return arg, 0


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('command')
    parser.add_argument('args', nargs='+')

    args = parser.parse_args()

    procs = []
    for arg in args.args:
        cmd = args.command.format(arg=arg)
        cmd = f'set -eo pipefail; ({cmd}) 2>&1 | tee {arg}.log | sed -e \'s,^,{arg}: ,\''
        procs.append((arg, ['bash', '-c', cmd]))

    with ThreadPoolExecutor(max_workers=3) as executor:
        future = executor.map(process, procs, timeout=TIMEOUT)
        procs = list(future)

    for arg, returncode in procs:
        if returncode > 0:
            log = f'{arg} failed with returncode {returncode}'
        else:
            log = f'{arg} succeeded with returncode {returncode}'
        with open(f'{arg}.log', mode='a') as fd:
            fd.write(log + '\n')
        print(log)


if __name__ == '__main__':
    main()
