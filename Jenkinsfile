@Library('eo-jenkins-lib@main') import eo.Utils

pipeline {
    agent any
    options {
        disableConcurrentBuilds()
        disableResume()
    }
    parameters {
        booleanParam(name: 'KEEP_VM', defaultValue: false, description: 'Do not shutdown the VM')
    }
    environment {
        VM_MEMORY = '8G'
		KEEP_VM = "${params.KEEP_VM}"
    }
    stages {
        stage('tests') {
            steps {
                sh 'rm -f *.ext4 *.log && python3 -u $PWD/parallel.py "$PWD/test-debvm {arg} -snapshot" bookworm trixie sid'
                stash(name: 'logs', includes: '*.log')
            }
        }
        stage('bookworm') {
            steps {
                unstash('logs')
                catchError(stageResult: 'FAILURE') {
                    sh 'cat bookworm.log && tail bookworm.log | grep -q "bookworm succeeded"'
                }
            }
        }
        stage('trixie') {
            steps {
                unstash('logs')
                catchError(stageResult: 'FAILURE') {
                    sh 'cat trixie.log && tail trixie.log | grep -q "trixie succeeded"'
                }
            }
        }
        stage('sid') {
            steps {
                unstash('logs')
                warnError('SID stage failed') {
                    sh 'cat sid.log && tail sid.log | grep -q "sid succeeded"'
                }
             }
        }
    }
    post {
        always {
            script {
                utils = new Utils()
                utils.mail_notify(currentBuild, env, 'ci+jenkins-publik-devinst@entrouvert.org')
            }
        }
        success {
            cleanWs()
        }
    }
}
